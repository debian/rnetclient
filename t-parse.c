/*
 *  Copyright (C) 2016  Gabriel F. T. Gomes <gabriel@gftg.com.br>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>

#include "decfile.h"
#include "rnet_encode.h"

int main(void)
{
	int i, r;
	struct rnet_decfile *decfile;
	struct rnet_message *message = NULL;
	char *filename[4] = {
		SRCDIR"/data/12345678909-IRPF-A-2013-2012-ORIGI.DEC",
		SRCDIR"/data/12345678909-IRPF-A-2014-2013-ORIGI.DEC",
		SRCDIR"/data/12345678909-IRPF-A-2015-2014-ORIGI.DEC",
		SRCDIR"/data/12345678909-IRPF-A-2016-2015-ORIGI.DEC"
	};

	for (i = 0; i < 4; i++) {

		decfile = rnet_decfile_open(filename[i]);
		if (decfile == NULL) {
			fprintf(stderr, "Error opening DEC file.\n");
			return -1;
		}

		r = rnet_encode(decfile, &message, NULL);
		if (r < 0) {
			fprintf(stderr, "Error encoding message\n");
			return -1;
		}

		rnet_message_del(message);
	}

	return 0;
}
